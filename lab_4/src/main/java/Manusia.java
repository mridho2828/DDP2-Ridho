public class Manusia {

    private String nama;
    private int umur;
    private int uang;
    private double kebahagiaan = 50.0;
    private Boolean kehidupan = true;
    private static Manusia terakhir;
    static final double MINIMUM = 0.0;
    static final double MAKSIMUM = 100.0;
    static final int USIA_KERJA_MINIMUM = 18;

    public Manusia(String nama, int umur, int uang) {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        terakhir = this;
    }

    public Manusia(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        this.uang = 50000;
        terakhir = this;
    }

    public String toString() {
        String ret = "";
        if (kehidupan) ret += "Nama\t\t: " + nama + "\n";
        else ret += "Nama\t\t: Almarhum " + nama + "\n";
        ret += "Umur\t\t: " + umur + "\n";
        ret += "Uang\t\t: " + uang + "\n";
        ret += "Kebahagiaan\t: " + kebahagiaan;
        return ret;
    }

    public void meninggal() {
        if (!kehidupan) {
            System.out.println(nama + " telah tiada");
            return;
        }
        kehidupan = false;
        System.out.println(nama + " meninggal dengan tenang, kebahagiaan : " + kebahagiaan);
        if (this == terakhir) {
            System.out.println("Semua harta " + nama + " hangus");
        }
        else {
            terakhir.uang += uang;
            System.out.println("Semua harta " + nama + " disumbangkan untuk " + terakhir.nama);
        }
        uang = 0;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public int getUmur() {
        return umur;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    public int getUang() {
        return uang;
    }

    public void setKebahagiaan(double kebahagiaan) {
        this.kebahagiaan = kebahagiaan;
    }

    public double getKebahagiaan() {
        return kebahagiaan;
    }

    public void setKehidupan(Boolean kehidupan) {
        this.kehidupan = kehidupan;
    }

    public Boolean getKehidupan() {
        return kehidupan;
    }

    public void beriUang(Manusia penerima) {
        if (!kehidupan) {
            if (!penerima.kehidupan) System.out.println(nama + " dan " + penerima.nama + " telah tiada");
            else System.out.println(nama + " telah tiada");
            return;
        }
        else if (!penerima.kehidupan) {
            System.out.println(penerima.nama + " telah tiada");
            return;
        }
        String namaPenerima = penerima.getNama();
        int jumlah = 0;
        for (int i = 0; i < namaPenerima.length(); ++i) {
            jumlah += (int)namaPenerima.charAt(i);
        }
        jumlah *= 100;
        if (uang >= jumlah) {
            kebahagiaan += jumlah / 6000.0;
            if (kebahagiaan > MAKSIMUM) kebahagiaan = MAKSIMUM;
            uang -= jumlah;

            penerima.kebahagiaan += jumlah / 6000.0;
            if (penerima.kebahagiaan > MAKSIMUM) penerima.kebahagiaan = MAKSIMUM;
            penerima.uang += jumlah;

            System.out.println(nama + " memberi uang sebanyak " + jumlah + " kepada " + namaPenerima + ", mereka berdua senang :D");
        }
        else {
            System.out.println(nama + " ingin memberi uang kepada " + namaPenerima + " namun tidak memiliki cukup uang :'(");
        }
    }

    public void beriUang(Manusia penerima, int jumlah) {
        if (!kehidupan) {
            if (!penerima.kehidupan) System.out.println(nama + " dan " + penerima.nama + " telah tiada");
            else System.out.println(nama + " telah tiada");
            return;
        }
        else if (!penerima.kehidupan) {
            System.out.println(penerima.nama + " telah tiada");
            return;
        }
        String namaPenerima = penerima.getNama();
        if (uang >= jumlah) {
            kebahagiaan += jumlah / 6000.0;
            if (kebahagiaan > MAKSIMUM) kebahagiaan = MAKSIMUM;
            uang -= jumlah;

            penerima.kebahagiaan += jumlah / 6000.0;
            if (penerima.kebahagiaan > MAKSIMUM) penerima.kebahagiaan = MAKSIMUM;
            penerima.uang += jumlah;

            System.out.println(nama + " memberi uang sebanyak " + jumlah + " kepada " + namaPenerima + ", mereka berdua senang :D");
        }
        else {
            System.out.println(nama + " ingin memberi uang kepada " + namaPenerima + " namun tidak memiliki cukup uang :'(");
        }               
    }

    public void sakit(String namaPenyakit) {
        if (!kehidupan) {
            System.out.println(nama + " telah tiada");
            return;
        }
        kebahagiaan -= namaPenyakit.length();
        if (kebahagiaan < MINIMUM) kebahagiaan = MINIMUM;
        System.out.println(nama + " terkena penyakit " + namaPenyakit + " :O");
    }

    public void bekerja(int durasi, int bebanKerja) {
        if (!kehidupan) {
            System.out.println(nama + " telah tiada");
            return;
        }
        if (umur < USIA_KERJA_MINIMUM) {
            System.out.println(nama + " belum boleh bekerja karena masih dibawah umur D:");
            return;
        }

        if (durasi * bebanKerja <= kebahagiaan) {
            int bebanKerjaTotal = durasi * bebanKerja;
            int pendapatan = bebanKerjaTotal * 10000;
            uang += pendapatan;
            kebahagiaan -= bebanKerjaTotal;
            System.out.println(nama + " bekerja full time, total pendapatan : " + pendapatan);
        }
        else {
            int durasiBaru = (int)(kebahagiaan / bebanKerja);
            int bebanKerjaTotal = durasiBaru * bebanKerja;
            int pendapatan = (int)(bebanKerjaTotal * 10000);
            uang += pendapatan;
            kebahagiaan -= bebanKerjaTotal;  
            System.out.println(nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
        }
    }

    public void rekreasi(String namaTempat) {
        if (!kehidupan) {
            System.out.println(nama + " telah tiada");
            return;
        }
        int biaya = namaTempat.length() * 10000;
        if (uang > biaya) {
            uang -= biaya;
            kebahagiaan += namaTempat.length();
            if (kebahagiaan > MAKSIMUM) kebahagiaan = MAKSIMUM;
            System.out.println(nama + " berekreasi di " + namaTempat + ", " + nama + " senang :)");
        }
        else {
            System.out.println(nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat + " :(");
        }
    }
}