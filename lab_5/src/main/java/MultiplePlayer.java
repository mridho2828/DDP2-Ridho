import java.util.Scanner;

public class MultiplePlayer {

	private static BingoCard[] cardList;

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String[] splitted = input.nextLine().split(" ");
		int n = Integer.parseInt(splitted[0]);
		cardList = new BingoCard[n];
		System.out.println(n);
		for (int k = 0; k < n; ++k) {
			Number[][] numbers = new Number[5][5];
			Number[] states = new Number[100];

			for (int i = 0; i < 5; ++i) {
				for (int j = 0; j < 5; ++j) {
					numbers[i][j] = new Number(input.nextInt(), i, j);
					states[numbers[i][j].getValue()] = numbers[i][j];
				}
			}
			cardList[k] = new BingoCard(numbers, states, splitted[k+1]);
		}
		boolean finish = false;
		while (!finish) {
			String command = input.next();
			if (command.equals("MARK")) {
				int num = input.nextInt();
				for (int i = 0; i < n; ++i) {
					System.out.println(cardList[i].getName() + ": " + cardList[i].markNum(num));
				}

				for (int i = 0; i < n; ++i) {
					if (cardList[i].isBingo()) {
						System.out.println(cardList[i].getName() + ": BINGO!");
						System.out.println(cardList[i].info());
						finish = true;
						break;
					}
				}
			}
			else if (command.equals("INFO")) {
				String name = input.next();
				for (int i = 0; i < n; ++i) {
					if (cardList[i].getName().equals(name)) {
						System.out.println(cardList[i].getName());
						System.out.println(cardList[i].info());
						break;
					}
				}
			}
			else if (command.equals("RESTART")) {
				String name = input.next();
				for (int i = 0; i < n; ++i) {
					if (cardList[i].getName().equals(name)) {
						if (cardList[i].isRepeat()) {
							System.out.println(name + " sudah pernah mengajukan RESTART");
						}
						else {
							for (int j = 0; j < n; ++j) cardList[j].restart();
							cardList[i].setRepeat(true);
						}
						break;
					}
				}
			}
			else {
				System.out.println("Incorrect command");
			}
		}
	}
}