/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengimplementasikanrjakan soal bonus tutorial lab 5
 * Anda diwajibkan untuk mengimplementasikan method yang masih kosong
 * Anda diperbolehkan untuk menambahkan method jika dibutuhkan
 * HINT : Bagaimana caranya cek apakah sudah menang atau tidak? Mungkin dibutuhkan method yang bisa membantu? Hmmmm.
 * Semangat ya :]
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	private String name;
	private boolean isRepeat;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public BingoCard(Number[][] numbers, Number[] numberStates, String name) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
		this.name = name;
		this.isRepeat = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}

	private boolean horizontalCheck() {
		for (int i = 0; i < 5; ++i) {
			boolean ok = true;
			for (int j = 0; j < 5; ++j) {
				if (!numbers[i][j].isChecked()) {
					ok = false;
					break;
				}
			}
			if (ok) return true;
		}
		return false;
	}

	private boolean verticalCheck() {
		for (int i = 0; i < 5; ++i) {
			boolean ok = true;
			for (int j = 0; j < 5; ++j) {
				if (!numbers[j][i].isChecked()) {
					ok = false;
					break;
				}
			}
			if (ok) return true;
		}
		return false;
	}

	private boolean diagonalCheck() {
		boolean ok = true;
		int x = 0, y = 0;
		for (int i = 0; i < 5; ++i) {
			if (!numbers[x][y].isChecked()) {
				ok = false;
				break;
			}
			++x; ++y;
		}
		if (ok) return true;

		ok = true;
		x = 0; y = 4;
		for (int i = 0; i < 5; ++i) {
			if (!numbers[x][y].isChecked()) {
				ok = false;
				break;
			}
			++x; --y;
		}
		if (ok) return true;

		return false;
	}

	private boolean checkBingo() {
		if (horizontalCheck() || verticalCheck() || diagonalCheck()) return true;
		return false;
	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String getName() {
		return name;
	}

	public boolean isRepeat() {
		return isRepeat;
	}

	public void setRepeat(boolean isRepeat) {
		this.isRepeat = isRepeat;
	}

	public String markNum(int num) {
		int x, y;
		try {
			x = numberStates[num].getX();
			y = numberStates[num].getY();
		}
		catch (NullPointerException e) {
			return "Kartu tidak memiliki angka " + num;
		}


		if (numbers[x][y].isChecked()) {
			return num + " sebelumnya sudah tersilang";
		}
		else {
			numbers[x][y].setChecked(true);
			if (checkBingo()) setBingo(true);
			return num + " tersilang";
		}
	}	
	
	public String info() {
		String status = "";
		for (int i = 0; i < 5; ++i) {
			status += "|";
			for (int j = 0; j < 5; ++j) {
				if (numbers[i][j].isChecked()) status += " X  |";
				else {
					if (numbers[i][j].getValue() < 10) status += " 0" + numbers[i][j].getValue() + " |";
					else status += " " + numbers[i][j].getValue() + " |";
				}
			}
			if (i < 4) status += "\n";
		}
		return status;
	}
	
	public void restart() {
		for (int i = 0; i < 5; ++i) {
			for (int j = 0; j < 5; ++j) {
				numbers[i][j].setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}
}