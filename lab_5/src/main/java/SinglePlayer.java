import java.util.Scanner;

public class SinglePlayer {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Number[][] numbers = new Number[5][5];
		Number[] states = new Number[100];

		for (int i = 0; i < 5; ++i) {
			for (int j = 0; j < 5; ++j) {
				numbers[i][j] = new Number(input.nextInt(), i, j);
				states[numbers[i][j].getValue()] = numbers[i][j];
			}
		}

		BingoCard card = new BingoCard(numbers, states);

		while (!card.isBingo()) {
			String command = input.next();
			if (command.equals("MARK")) {
				int num = input.nextInt();
				System.out.println(card.markNum(num));
			}
			else if (command.equals("INFO")) {
				System.out.println(card.info());
			}
			else if (command.equals("RESTART")) {
				card.restart();
			}
			else {
				System.out.println("Incorrect command");
			}
		}
		System.out.println("BINGO!");
		System.out.println(card.info());
	}
}