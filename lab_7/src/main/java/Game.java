import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    ArrayList<Player> eaten = new ArrayList<Player>();
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player instanceOfPlayer : player) {
            if (instanceOfPlayer.getName().equals(name)) {
                return instanceOfPlayer;
            }
        }
        return null;
    }

    public int findIndex(String name) {
        for (int i = 0; i < player.size(); ++i) {
            if (player.get(i).getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public int findIndexDiet(String name) {
        for (int i = 0; i < eaten.size(); ++i) {
            if (eaten.get(i).getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        System.out.println("=======================================");
        if (find(chara) != null) return "Sudah ada karakter bernama " + chara;
        if (tipe.equals("Monster")) player.add(new Monster(chara, hp));
        else if (tipe.equals("Magician")) player.add(new Magician(chara, hp));
        else player.add(new Human(chara, hp));
        if (findIndexDiet(chara) != -1) eaten.remove(findIndexDiet(chara));
        return chara + " ditambah ke game";
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        System.out.println("=======================================");
        if (find(chara) != null) return "Sudah ada karakter bernama " + chara;
        if (tipe.equals("Magician")) return "Magician tidak bisa berteriak";
        if (tipe.equals("Human")) return "Human tidak bisa berteriak";
        player.add(new Monster(chara, hp, roar));
        if (findIndexDiet(chara) != -1) eaten.remove(findIndexDiet(chara));
        return chara + " ditambah ke game";
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        System.out.println("=======================================");
        if (find(chara) == null) return "Tidak ada " + chara;
        int index = findIndex(chara);
        player.remove(index);
        return chara + " dihapus dari game";
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        System.out.println("=======================================");
        Player instanceOfPlayer = find(chara);
        if (instanceOfPlayer == null) return "Tidak ada " + chara;
        else return instanceOfPlayer.status();
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        System.out.println("=======================================");
        String ret = "";
        if (player.size() == 0) return "Tidak ada pemain";
        for (Player instanceOfPlayer : player) ret += instanceOfPlayer.status() + "\n";
        return ret;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        System.out.println("=======================================");
        Player instanceOfPlayer = find(chara);
        if (instanceOfPlayer == null) return "Tidak ada " + chara;
        else return instanceOfPlayer.diet();
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        System.out.println("=======================================");
        String ret = "";
        if (eaten.size() == 0) return "Belum ada yang termakan";
        for (Player instanceOfPlayer : eaten) ret += instanceOfPlayer.diet() + "\n";
        return ret;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        System.out.println("=======================================");
        Player attacker = find(meName);
        Player target = find(enemyName);
        if (attacker == null || target == null) return "Tidak ada " + meName + " atau " + enemyName;
        return attacker.attack(target);
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        System.out.println("=======================================");
        Player attacker = find(meName);
        Player target = find(enemyName);
        if (attacker == null || target == null) return "Tidak ada " + meName + " atau " + enemyName;
        return attacker.burn(target);
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        System.out.println("=======================================");
        Player attacker = find(meName);
        Player target = find(enemyName);
        if (attacker == null || target == null) return "Tidak ada " + meName + " atau " + enemyName;
        if (attacker.eat(target)) {
            eaten.add(target);
            int index = findIndex(enemyName);
            player.remove(index);
            return meName + " memakan " + enemyName + "\nNyawa " + meName + " kini " + attacker.getHp();
        }
        else {
            return meName + " tidak bisa memakan " + enemyName;
        }
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        System.out.println("=======================================");
        Player instanceOfPlayer = find(meName);
        if (instanceOfPlayer == null) return "Tidak ada " + meName;
        return instanceOfPlayer.roar();
    }
}