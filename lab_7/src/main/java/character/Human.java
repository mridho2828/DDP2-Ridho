package character;

public class Human extends Player {

	public Human(String name, int hp) {
		super(name, Math.max(hp, 0));
	}

	public boolean canEat(Player target) {
		if (target.getType().equals("Monster")) {
			if (target.isBurned()) return true;
			return false;
		}
		return false;
	}

	public String getType() {
		return "Human";
	}

	public String roar() {
		return this.getName() + " tidak bisa berteriak";
	}

	public String burn(Player other) {
		return this.getName() + " tidak bisa membakar";
	}
}