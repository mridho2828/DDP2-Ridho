package character;

public class Magician extends Human {

	public Magician(String name, int hp) {
		super(name, Math.max(0, hp));
	}

	public String getType() {
		return "Magician";
	}

	public String burn(Player target) {
		if (target.getType().equals("Magician")) target.addHp(-20);
		else target.addHp(-10);
		if (target.isDead()) {
			target.setBurnedTrue();
			return "Nyawa " + target.getName() + " " + target.getHp() + " \n dan matang";
		}
		return "Nyawa " + target.getName() + " " + target.getHp();
	}
}