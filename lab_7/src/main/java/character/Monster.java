package character;

public class Monster extends Player {

	String sound = null;

	public Monster(String name, int hp) {
		super(name, Math.max(0, hp * 2));
	}

	public Monster(String name, int hp, String sound) {
		super(name, Math.max(0, hp * 2));
		this.sound = sound;
	}

	public boolean canEat(Player target) {
		if (target.isDead()) return true;
		return false;
	}

	public String getType() {
		return "Monster";
	}

	public String roar() {
		if (sound == null) return "AAAAAAaaaAAAAAaaaAAAAAA";
		else return sound;
	}

	public String burn(Player target) {
		return this.getName() + " tidak bisa membakar";
	}
}