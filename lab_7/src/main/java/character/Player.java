package character;

import java.util.ArrayList;
import java.lang.Math;

public abstract class Player {

	private String name;
	private int hp;
	private ArrayList<Player> eaten = new ArrayList<Player>();
	private boolean isBurned = false;

	public Player(String name, int hp) {
		this.name = name;
		this.hp = hp;
	}

	public abstract boolean canEat(Player target);
	public abstract String getType();
	public abstract String roar();
	public abstract String burn(Player target);

	public String getName() { return name; }
	public int getHp() { return hp; }
	public boolean isBurned() { return isBurned; }
	public void setBurnedTrue() { isBurned = true; }
	public String getTypeName() { return getType() + " " + getName(); }
	public boolean isDead() { return hp <= 0; }

	public String diet() {
		String ret = "";
		if (eaten.size() == 0) return "Belum memakan siapa siapa";
		else {
			ret += "Memakan ";
			for (int i = 0; i < eaten.size(); ++i) {
				if (i > 0) ret += ", ";
				ret += eaten.get(i).getTypeName();
			}
		}
		return ret;
 	}

	public String status() {
		String ret = getTypeName() + "\n";
		ret += "HP: " + getHp() + "\n";
		if (isDead()) ret += "Sudah meninggal dunia dengan damai\n";
		else ret += "Masih hidup\n";
		ret += diet();
		return ret;
	}
	
	protected void addHp(int hp) {
		this.hp += hp;
		this.hp = Math.max(this.hp, 0);
	}

	public String attack(Player target) {
		if (target.getType().equals("Magician")) target.addHp(-20);
		else target.addHp(-10);
		return "Nyawa " + target.getName() + " " + target.getHp();
	}

	public boolean eat(Player target) {
		if (canEat(target)) {
			this.addHp(15);
			eaten.add(target);
			return true;
		}
		return false;
	}
}