package movie;

public class Movie {

	String judulMovie;
	String rating;
	int durasi;
	String genre;
	String jenis;

	public Movie(String judulMovie, String rating, int durasi, String genre, String jenis) {
		this.judulMovie = judulMovie;
		this.rating = rating;
		this.durasi = durasi;
		this.genre = genre;
		this.jenis = jenis;
	}

	public String getJudulMovie() { return judulMovie; }
	public String getRating() { return rating; }

	public int getUmurMinimal() {
		if (rating.equals("Umum")) return 0;
		if (rating.equals("Remaja")) return 13;
		return 17;
	}

	public String getInfo() {
		String info = "------------------------------------------------------------------\n";
		info += "Judul   : " + judulMovie + "\n";
		info += "Genre   : " + genre + "\n";
		info += "Durasi  : " + durasi + " menit\n";
		info += "Rating  : " + rating + "\n";
		info += "Jenis   : Film " + jenis + "\n";
		info += "------------------------------------------------------------------";
		return info;
	}

	public boolean equals(Movie otherMovie) {
		if (judulMovie.equals(otherMovie.getJudulMovie())) return true;
		return false;
	}
}