package customer;

import java.util.ArrayList;
import movie.*;
import ticket.*;
import theater.*;

public class Customer {

	String namaCustomer;
	String jenisKelamin;
	int umurCustomer;
	ArrayList<Ticket> ticketList = new ArrayList<Ticket>();

	public Customer(String namaCustomer, String jenisKelamin, int umurCustomer) {
		this.namaCustomer = namaCustomer;
		this.jenisKelamin = jenisKelamin;
		this.umurCustomer = umurCustomer;
	}

	public Ticket orderTicket(Theater theater, String judul, String jadwal, String jenis) {
		boolean is3D = jenis.equals("3 Dimensi") ? true : false;
		ArrayList<Ticket> dafarTiket = theater.getDaftarTiket();
		for (Ticket t : dafarTiket) {
			if (t.getFilm().getJudulMovie().equals(judul) && t.getJadwal().equals(jadwal) && t.is3D() == is3D) {
				if (umurCustomer >= t.getFilm().getUmurMinimal()) {
					System.out.println(namaCustomer + " telah membeli tiket " + judul + " jenis " + jenis +
					" di " + theater.getNamaTheater() + " pada hari " + jadwal + " seharga Rp. " + t.getHarga());

					theater.addSaldo(t.getHarga());
					ticketList.add(t);
					return t;
				}
				else {
					System.out.println(namaCustomer + " masih belum cukup umur untuk menonton " + judul + 
					" dengan rating " + t.getFilm().getRating());
					return null;
				}
			}
		}
		System.out.println("Tiket untuk film " + judul + " jenis " + jenis + " dengan jadwal " + 
		jadwal + " tidak tersedia di " + theater.getNamaTheater());
		return null;
	}

	public void findMovie(Theater theater, String judul) {
		Movie[] daftarFilm = theater.getDaftarFilm();
		for (Movie m : daftarFilm) {
			if (m.getJudulMovie().equals(judul)) {
				System.out.println(m.getInfo());
				return;
			}
		}
		System.out.println("Film " + judul + " yang dicari " + namaCustomer
		+ " tidak ada di bioskop " + theater.getNamaTheater());
	}

	public void cancelTicket(Theater theater) {
		Ticket lastTicket = ticketList.get(ticketList.size() - 1);
		if (lastTicket.isUsed()) {
			System.out.println("Tiket tidak bisa dikembalikan karena film " + lastTicket.getFilm().getJudulMovie()
			+ " sudah ditonton oleh " + namaCustomer);
			return;
		}

		if (theater.checkMovies(lastTicket.getFilm())) {
			if (theater.getSaldo() < lastTicket.getHarga()) {
				System.out.println("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop "
				+ theater.getNamaTheater() + " lagi tekor...");
			}
			else {
				System.out.println("Tiket film " + lastTicket.getFilm().getJudulMovie() + " dengan waktu tayang " +
				lastTicket.getJadwal() + " jenis " + (lastTicket.is3D() ? "3 Dimensi" : "Biasa") +
				" dikembalikan ke bioskop " + theater.getNamaTheater());
				theater.addSaldo(-lastTicket.getHarga());
				ticketList.remove(ticketList.size() - 1);
			}
		}
		else {
			System.out.println("Maaf tiket tidak bisa dikembalikan, " + lastTicket.getFilm().getJudulMovie()
			+ " tidak tersedia dalam " + theater.getNamaTheater());
		}
	}

	public void watchMovie(Ticket tiket) {
		System.out.println(namaCustomer + " telah menonton film " + tiket.getFilm().getJudulMovie());
		tiket.setUsed(true);
	}
}