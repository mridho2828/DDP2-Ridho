package ticket;

import movie.*;

public class Ticket {

	Movie film;
	String jadwal;
	boolean is3D;
	boolean isUsed = false;

	public Ticket(Movie film, String jadwal, boolean is3D) {
		this.film = film;
		this.jadwal = jadwal;
		this.is3D = is3D;
	}

	public Movie getFilm() { return film; }
	public String getJadwal() { return jadwal; }
	public boolean is3D() { return is3D; }
	public boolean isUsed() { return isUsed; }
	public void setUsed(boolean isUsed) { this.isUsed = isUsed; }

	public int getHarga() {
		int harga;
		if (jadwal.equals("Sabtu") || jadwal.equals("Minggu")) harga = 100000;
		else harga = 60000;
		if (is3D) harga += harga / 5;
		return harga;
	}

	public void printInfo() {
		System.out.println("------------------------------------------------------------------");
		System.out.println("Film            : " + film.getJudulMovie());
		System.out.println("Jadwal Tayang   : " + jadwal);
		System.out.println("Jenis           : " + (is3D ? "3 Dimensi" : "Biasa"));
		System.out.println("------------------------------------------------------------------");
	}
}