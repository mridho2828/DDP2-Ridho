package theater;

import java.util.ArrayList;
import movie.*;
import ticket.*;

public class Theater {

	String namaTheater;
	int saldo;
	int jumlahTiket;
	ArrayList<Ticket> daftarTiket;
	Movie[] daftarFilm;

	public Theater(String namaTheater, int saldo, ArrayList<Ticket> daftarTiket, Movie[] daftarFilm) {
		this.namaTheater = namaTheater;
		this.saldo = saldo;
		this.daftarTiket = daftarTiket;
		this.daftarFilm = daftarFilm;
	}

	public String getNamaTheater() { return namaTheater; }
	public int getSaldo() { return saldo; }
	public ArrayList<Ticket> getDaftarTiket() { return daftarTiket; }
	public Movie[] getDaftarFilm() { return daftarFilm; }
	public void addSaldo(int saldoTambahan) {
		saldo += saldoTambahan;
	}

	public boolean checkMovies(Movie otherMovie) {
		for (Movie m : daftarFilm) {
			if (m.equals(otherMovie)) return true;
		}
		return false;
	}

	public void printInfo() {
		System.out.println("------------------------------------------------------------------");
		System.out.println("Bioskop                 : " + namaTheater);
		System.out.println("Saldo Kas               : " + saldo);
		System.out.println("Jumlah tiket tersedia   : " + daftarTiket.size());
		System.out.print("Daftar Film tersedia    : ");
		for (int i = 0; i < daftarFilm.length; ++i) {
			System.out.print(daftarFilm[i].getJudulMovie());
			if (i == daftarFilm.length - 1) System.out.print("\n");
			else System.out.print(", ");
		}
		System.out.println("------------------------------------------------------------------");
	}

	public static void printTotalRevenueEarned(Theater[] theaters) {
		int sumOfRevenue = 0;
		for (Theater t : theaters) sumOfRevenue += t.saldo;
		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + sumOfRevenue);
		System.out.println("------------------------------------------------------------------");
		for (Theater t : theaters) {
			System.out.println("Bioskop         : " + t.namaTheater);
			System.out.println("Saldo Kas       : Rp. " + t.saldo);
			System.out.println();
		}
		System.out.println("------------------------------------------------------------------");
	}
}