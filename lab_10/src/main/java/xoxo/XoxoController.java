package xoxo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import xoxo.crypto.*;
import xoxo.exceptions.*;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Muhammad Ridho Ananda
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * The file in which the encrypted message will be written.
     */
    private File encrpytionResult;

    /**
     * File writer object of the encryption message.
     */
    private FileWriter encryptionWriter;

    /**
     * The file in which the decrypted message will be written.
     */
    private File decryptionResult;

    /**
     * File writer object of the decrypted message.
     */
    private FileWriter decryptionWriter;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        try {
            initFile();
        }
        catch (IOException e) {
            gui.exceptionHandler("Exception at files initializer");
            System.exit(0);
        }
        setEncryptController();
        setDecryptController();
        gui.startGui();
    }

    /**
     * Initialize files to be written
     * @throws IOException
     */
    private void initFile() throws IOException {
        encrpytionResult = new File("Encrpytion Result.enc");
        encrpytionResult.createNewFile();
        encryptionWriter = new FileWriter(encrpytionResult);

        decryptionResult = new File("Decryption Result.txt");
        decryptionResult.createNewFile();
        decryptionWriter = new FileWriter(decryptionResult);
    }

    /**
     * implement setEncryptFunction from XoxoView
     */
    private void setEncryptController() {
        gui.setEncryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                XoxoEncryption encrypter;
                try {
                    encrypter = new XoxoEncryption(gui.getKeyText());
                }
                catch (KeyTooLongException e) {
                    gui.exceptionHandler("Key to encrypt must not exceed 28 in length");
                    return;
                }

                try {
                    int seedInt;
                    if (gui.getSeedText().length() == 0) seedInt = HugKey.DEFAULT_SEED;
                    else seedInt = Integer.parseInt(gui.getSeedText());
                    XoxoMessage message = encrypter.encrypt(gui.getMessageText(), seedInt);
                    gui.appendLog("The encrypted message is : " + message.getEncryptedMessage());
                    encryptionWriter.write(message.getEncryptedMessage() + "\n");
                    encryptionWriter.flush();
                }
                catch (NumberFormatException e) {
                    gui.exceptionHandler("Seed must be an integer");
                }
                catch (RangeExceededException e) {
                    gui.exceptionHandler("Seed must be between 0 to 36");
                }
                catch (SizeTooBigException e) {
                    gui.exceptionHandler("Message must not exceed 10 kbit");
                }
                catch (InvalidCharacterException e) {
                    gui.exceptionHandler("Key to encrypt must contain valid characters only ('@', 'A-Z', 'a-z')");
                }
                catch (IOException e) {
                    gui.exceptionHandler("Can't write encrypted message to the file");
                }
            }
        });
    }

    /**
     * implement setDecryptFunction from XoxoView
     */
    private void setDecryptController() {
        gui.setDecryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                XoxoDecryption decrypter = new XoxoDecryption(gui.getKeyText());
                try {
                    int seedInt;
                    if (gui.getSeedText().length() == 0) seedInt = HugKey.DEFAULT_SEED;
                    else seedInt = Integer.parseInt(gui.getSeedText());
                    String decryptedMessage = decrypter.decrypt(gui.getMessageText(), seedInt);
                    gui.appendLog("The decrypted message is : " + decryptedMessage);
                    decryptionWriter.write(decryptedMessage + "\n");
                    decryptionWriter.flush();
                }
                catch (NumberFormatException e) {
                    gui.exceptionHandler("Seed must be an integer");
                }
                catch (RangeExceededException e) {
                    gui.exceptionHandler("Seed must be between 0 to 36");
                }
                catch (SizeTooBigException e) {
                    gui.exceptionHandler("Message must not exceed 10 kbit");
                }
                catch (IOException e) {
                    gui.exceptionHandler("Can't write decrypted message to the file");
                }
            }
        });
    }
}