package xoxo.crypto;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

import java.io.UnsupportedEncodingException;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.
     */
    private KissKey kissKey;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     * 
     * @throws KeyTooLongException if the length of the
     *         kissKeyString exceeded 28 characters.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException {
        this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     */
    public XoxoMessage encrypt(String message) throws SizeTooBigException, InvalidCharacterException {
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @param seed A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if the given seed is less than 0 or more than 36
     */
    public XoxoMessage encrypt(String message, int seed)
            throws RangeExceededException, SizeTooBigException, InvalidCharacterException {
        if (seed < HugKey.MIN_RANGE || seed > HugKey.MAX_RANGE) {
            throw new RangeExceededException("Range of the seed must not exceed 0 to 36");
        }
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     * 
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException if given message's size is more than 10 kbit
     * @throws InvalidCharacterException if kiss key contains character outside '@', 'a-z', 'A-Z'
     */
    private String encryptMessage(String message) throws SizeTooBigException, InvalidCharacterException {
        try {
            if (message.getBytes("UTF-8").length * 8 > 10000) {
                throw new SizeTooBigException("Given message can't be larger than 10 kbit");
            }
        }
        catch (UnsupportedEncodingException e) {
            if (message.getBytes().length * 8 > 10000) {
                throw new SizeTooBigException("Given message can't be larger than 10 kbit");
            }
        }

        for (int i = 0; i < kissKey.length(); ++i) {
            int kiss = this.kissKey.keyAt(i);
            if (!(kiss == '@' || (kiss >= 'a' && kiss <= 'z') || (kiss >= 'A' && kiss <= 'Z'))) {
                throw new InvalidCharacterException("Key must not contain invalid characters");
            }
        }

        final int length = message.length();
        String encryptedMessage = "";
        for (int i = 0; i < length; i++) {
            int m = message.charAt(i);
            int kiss = this.kissKey.keyAt(i);
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
        return encryptedMessage;
    }
}
