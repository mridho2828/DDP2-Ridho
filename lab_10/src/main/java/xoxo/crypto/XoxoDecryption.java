package xoxo.crypto;

import xoxo.exceptions.*;
import xoxo.key.HugKey;

import java.io.UnsupportedEncodingException;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     * @throws SizeTooBigException if given message's size is larger than 10 kbit.
     * @throws RangeExceededException if given seed is less than 0 or more than 36.
     */
    public String decrypt(String encryptedMessage, int seed) throws SizeTooBigException, RangeExceededException {
        if (seed < HugKey.MIN_RANGE || seed > HugKey.MAX_RANGE) {
            throw new RangeExceededException("Range of the seed must not exceed 0 to 36");
        }

        try {
            if (encryptedMessage.getBytes("UTF-8").length * 8 > 10000) {
                throw new SizeTooBigException("Given message can't be larger than 10 kbit");
            }
        }
        catch (UnsupportedEncodingException e) {
            if (encryptedMessage.getBytes().length * 8 > 10000) {
                throw new SizeTooBigException("Given message can't be larger than 10 kbit");
            }
        }
        String decryptedMessage = "";
        final int length = encryptedMessage.length();
        for (int i = 0; i < length; ++i) {
            int k = (this.hugKeyString.charAt(i % hugKeyString.length()) ^ seed) - 'a';
            int value = encryptedMessage.charAt(i) ^ k;
            decryptedMessage += (char)value;
        }
        return decryptedMessage;
    }
}