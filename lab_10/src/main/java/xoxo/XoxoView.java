package xoxo;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Muhammad Ridho Ananda
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * A button that when it is clicked, it clears the log field.
     */
    private JButton clearLogButton;

    /**
     * Frame of the whole GUI.
     */
    private JFrame frame;

    /**
     * A label that describes what to be written at key field.
     */
    private JLabel messageLabel;

    /**
     * A label that describes what to be written at message field.
     */
    private JLabel keyLabel;

    /**
     * A label that describes what to be written at seed field.
     */
    private JLabel seedLabel;

    /**
     * A label that describes what is written at log field.
     */
    private JLabel logLabel;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        messageField = new JTextField();
        messageField.setPreferredSize(new Dimension(500, 50));
        messageLabel = new JLabel("Write your message here");

        keyField = new JTextField();
        keyField.setPreferredSize(new Dimension(500, 50));
        keyLabel = new JLabel("Write your secret key here");

        seedField = new JTextField();
        seedField.setPreferredSize(new Dimension(500, 50));
        seedLabel = new JLabel("Write your seed number here");

        logField = new JTextArea();
        logField.setPreferredSize(new Dimension(500, 50));
        logLabel = new JLabel("Result");

        encryptButton = new JButton("encrypt");
        decryptButton = new JButton("decrypt");
        clearLogButton = new JButton("clear");
        clearLogButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                logField.setText("");
            }
        });

        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.Y_AXIS));
        textPanel.add(messageLabel);
        textPanel.add(messageField);
        textPanel.add(keyLabel);
        textPanel.add(keyField);
        textPanel.add(seedLabel);
        textPanel.add(seedField);
        textPanel.add(logLabel);
        textPanel.add(logField);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());
        buttonPanel.add(encryptButton);
        buttonPanel.add(decryptButton);
        buttonPanel.add(clearLogButton);

        Container container = new Container();
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.add(textPanel);
        container.add(buttonPanel);

        frame = new JFrame();
        frame.add(container);
    }

    /**
     * display GUI and enable user to interact with the GUI
     */
    public void startGui() {
        frame.setTitle("Xoxo Encrypter And Decrypter");
        frame.setPreferredSize(new Dimension(600, 600));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * show message to user if an exception occurs
     * @param message the exception message
     */
    public void exceptionHandler(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}