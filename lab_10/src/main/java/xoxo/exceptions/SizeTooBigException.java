package xoxo.exceptions;

/**
 * An exception that is thrown if a given message
 * has size more than 10 kbit
 */
public class SizeTooBigException extends RuntimeException {

    public SizeTooBigException(String message) {
        super(message);
    }
}