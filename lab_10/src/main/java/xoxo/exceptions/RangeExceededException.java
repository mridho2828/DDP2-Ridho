package xoxo.exceptions;

/**
 * An exception that is thrown if a seed number
 * is less than 0 or more than 36
 */
public class RangeExceededException extends RuntimeException {

    /**
     * Class constructor
     * @param message
     */
    public RangeExceededException(String message) {
        super(message);
    }

}