package xoxo.exceptions;

/**
 * An exception that is thrown if a kiss key that is used to encrypt a message
 * contains a character outside '@', 'A-Z', 'a-z'
 */
public class InvalidCharacterException extends RuntimeException {

    /**
     * Class constructor
     * @param message
     */
    public InvalidCharacterException(String message) {
        super(message);
    }
}