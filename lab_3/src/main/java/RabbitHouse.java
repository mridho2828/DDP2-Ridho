import java.util.Scanner;

public class RabbitHouse {

	// fungsi rekursif yang menghitung banyaknya keturunan dari 
	// suatu kelinci yang memiliki panjang nameLength (termasuk dia sendiri)
	public static int normalCount(int nameLength) {
		if (nameLength == 1) {
			return 1;
		}
		else {
			return 1 + nameLength * normalCount(nameLength - 1);
		}
	}

	// fungsi mengecek apakah suatu string palindrom
	public static boolean isPalindrome(String S) {
		for (int i = 0; i < S.length(); ++i) {
			if (S.charAt(i) != S.charAt(S.length()-1-i)) {
				return false;
			}
		}
		return true;
	}

	// fungsi rekursif yang menghitung banyaknya keturunan dari kelinci yang
	// bername name (termasuk dia sendiri)
	// namun memiliki palindromitis
	public static int palindromeCount(String name) {
		if (isPalindrome(name)) {
			return 0;
		}
		else {
			int result = 1;
			for (int i = 0; i < name.length(); ++i) {
				String front = (i == 0 ? "" : name.substring(0, i));
				String back = (i == name.length()-1 ? "" : name.substring(i+1, name.length()));
				result += palindromeCount(front+back);
			}
			return result;
		}
	}

	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		String fromUser = input.nextLine();
		String command = fromUser.split(" ")[0];
		String rabbitName = fromUser.split(" ")[1];

		if (command.equals("normal")) {
			System.out.println(normalCount(rabbitName.length()));
		}
		else if (command.equals("palindrom")) {
			System.out.println(palindromeCount(rabbitName));
		}
		else {
			System.out.println("perintah tidak valid!");
		}
	}
}