package lab9.event;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.math.BigInteger;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable {

    /**
     * Name of the event
     */
    private String name;
    /**
     * Start time of the event
     */
    private LocalDateTime startTime;
    /**
     * End time of the event, guaranteed to occur not before the start time
     */
    private LocalDateTime endTime;
    /**
     * Cost of the event
     */
    private BigInteger costPerHour;

    /**
     * convert String type to LocalDateTime
     * @param timeStr String type of the time, format yyyy-mm-dd_hh:mm:ss
     * @return LocalDateTime instance of given time
     */
    public static LocalDateTime getDateTime(String timeStr) {
        String date = timeStr.split("_")[0];
        String time = timeStr.split("_")[1];
        return LocalDateTime.of(
                Integer.parseInt(date.split("-")[0]),
                Integer.parseInt(date.split("-")[1]),
                Integer.parseInt(date.split("-")[2]),
                Integer.parseInt(time.split(":")[0]),
                Integer.parseInt(time.split(":")[1]),
                Integer.parseInt(time.split(":")[2])
        );
    }

    /**
     * convert LocalDateTime to String type with given format
     * @param parsedTime LocalDateTime instance
     * @return String type with format dd-mm-yyyy, hh:mm:ss
     */
    public static String getTimeStr(LocalDateTime parsedTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss");
        return parsedTime.format(formatter);
    }

    /**
     * Constructor
     * Initialize event with given parameters
     * @param name name of the event
     * @param startTimeStr start time of the event
     * @param endTimeStr end time of the event
     * @param costPerHourStr cost of the event
     */
    public Event(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        this.name = name;
        this.startTime = Event.getDateTime(startTimeStr);
        this.endTime = Event.getDateTime(endTimeStr);
        this.costPerHour = new BigInteger(costPerHourStr);
    }

    /**
     * Accessor for name of the event
     * @return name of this instance
     */
    public String getName() {
        return this.name;
    }

    /**
     * Accessor for start time of the event
     * @return start time of this instance
     */
    public LocalDateTime getStartTime() {
        return this.startTime;
    }

    /**
     * Accessor for end time of the event
     * @return end time of this instance
     */
    public LocalDateTime getEndTime() {
        return this.endTime;
    }

    /**
     * Accessor for cost of the event
     * @return cost of this instance
     */
    public BigInteger getCostPerHour() {
        return this.costPerHour;
    }

    /**
     * String describing the event
     * @return printable string of this instance
     */
    @Override
    public String toString() {
        return(
                name + "\n" +
                "Waktu mulai: " + getTimeStr(startTime) + "\n" +
                "Waktu selesai: " + getTimeStr(endTime) + "\n" +
                "Biaya kehadiran: " + costPerHour.toString()
        );
    }

    /**
     * Method to check whether this instance overlaps with other event
     * @param other instance of another event
     * @return true if overlaps, false otherwise
     */
    public boolean overlapsWith(Event other) {
        if (this.endTime.minusSeconds(1).isBefore(other.getStartTime()) ||
                other.getEndTime().minusSeconds(1).isBefore(this.startTime)) return false;
        return true;
    }

    /**
     * implements compareTo method from Comparable interface
     * @param other instance of another event
     * @return -1, 0, 1, if this event occurs before, at the same time, or after the other event respectively
     */
    @Override
    public int compareTo(Object other) {
        Event otherEvent = (Event)other;
        if (this.startTime.isBefore(otherEvent.getStartTime())) return -1;
        else if (this.startTime.equals(otherEvent.getStartTime())) return 0;
        else return 1;
    }
}
