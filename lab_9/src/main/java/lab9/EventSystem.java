package lab9;

import lab9.user.User;
import lab9.event.Event;
import java.util.ArrayList;

/**
* Class representing event managing system
*/
public class EventSystem {
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;

    /**
     * Method to check whether there is a user with a given name
     * @param name given name to be looked up
     * @return instance of the user if there is a user with a given name, null otherwise
     */
    public User getUser(String name) {
        for (User addedUser : users) {
            if (name.equalsIgnoreCase(addedUser.getName())) {
                return addedUser;
            }
        }
        return null;
    }

    /**
     * Method to check whether there is an event with a given name
     * @param name given name to be looked up
     * @return instance of the event if there is an event with a given name, null otherwise
     */
    public Event getEvent(String name) {
        for (Event addedEvent : events) {
            if (name.equalsIgnoreCase(addedEvent.getName())) {
                return addedEvent;
            }
        }
        return null;
    }

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Add an event to the system
     * @param name name of the event
     * @param startTimeStr start time of the event
     * @param endTimeStr end time of the event
     * @param costPerHourStr cost of the event
     * @return printable String describing the result of this method
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        if (getEvent(name) != null) return "Event " + name + " sudah ada!";
        if (Event.getDateTime(startTimeStr).isAfter(Event.getDateTime(endTimeStr))) {
            return "Waktu yang diinputkan tidak valid!";
        }
        events.add(new Event(name, startTimeStr, endTimeStr, costPerHourStr));
        return "Event " + name + " berhasil ditambahkan!";
    }

    /**
     * Add a user to the system
     * @param name name of the user
     * @return printable String describing the result of this method
     */
    public String addUser(String name) {
        if (getUser(name) != null) return "User " + name + " sudah ada!";
        users.add(new User(name));
        return "User " + name + " berhasil ditambahkan!";
    }

    /**
     * Add an event to a user
     * @param userName name of the user
     * @param eventName name of the event
     * @return printable String describing the result of this method
     */
    public String registerToEvent(String userName, String eventName) {
        User user = getUser(userName);
        Event event = getEvent(eventName);
        if (user == null && event == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        }
        if (event == null) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        }
        if (user == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        }

        if (user.addEvent(event)) {
            return userName + " berencana menghadiri " + eventName + "!";
        }
        else {
            return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
        }
    }
}
