import java.util.Scanner;

import korporasi.*;

// Asumsi bahwa nama karyawan hanya terdiri dari 1 kata

class Lab8 {

	private static void queryHandler(String query) {
		String[] command = query.split(" ");
    	try {
	    	if (command[0].equalsIgnoreCase("TAMBAH_KARYAWAN")) {
		   		System.out.println(
		   			Korporasi.addKaryawan(command[1], command[2], Integer.parseInt(command[3]))
		   		);
	   		}
	    	else if (command[0].equalsIgnoreCase("STATUS")) {
	    		System.out.println(Korporasi.status(command[1]));
	    	}
	    	else if (command[0].equalsIgnoreCase("TAMBAH_BAWAHAN")) {
	    		System.out.println(Korporasi.addBawahan(command[1], command[2]));
	    	}
	    	else if (command[0].equalsIgnoreCase("GAJIAN")) {
	    		Korporasi.gajian();
	   		}
	   		else {
	   			System.out.println("PERINTAH TIDAK VALID");
	   		}
	   	}
	   	catch (Exception e) {
	   		System.out.println("PERINTAH TIDAK VALID");
	   	}	
	}

    public static void main(String[] args) {
    	Scanner input = new Scanner(System.in);
    	String firstInput = input.nextLine();
    	try {
    		int maxGajiStaff = Integer.parseInt(firstInput);
    		Korporasi.setMaxGajiStaff(maxGajiStaff);
    	}
    	catch (Exception e) {
    		queryHandler(firstInput);
    	}

    	while (true) {
    		queryHandler(input.nextLine());
    	}
    }
}