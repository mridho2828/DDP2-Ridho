package korporasi;

import karyawan.*;
import java.util.ArrayList;

public class Korporasi {

	private static final int MAX_KARYAWAN = 10000;
	private static ArrayList<Karyawan> listKaryawan = new ArrayList<Karyawan>();
	private static int maxGajiStaff = 18000;

	public static Karyawan cariKaryawan(String nama) {
		for (Karyawan karyawan : listKaryawan) {
			if (karyawan.getNama().equalsIgnoreCase(nama)) {
				return karyawan;
			}
		}
		return null;
	}

	public static void setMaxGajiStaff(int gaji) {
		maxGajiStaff = gaji;
	}

	public static String addKaryawan(String tipe, String nama, int gaji) {
		if (listKaryawan.size() == MAX_KARYAWAN) {
			return "Jumlah karyawan tidak bisa ditambah lagi";
		}
		if (cariKaryawan(nama) != null) {
			return "Karyawan dengan nama " + nama + " telah terdaftar";
		}

		if (tipe.equalsIgnoreCase("MANAGER")) {
			listKaryawan.add(new Manager(nama, gaji));
		}
		else if (tipe.equalsIgnoreCase("STAFF")) {
			if (gaji > maxGajiStaff) return "Gaji awal untuk staff terlalu banyak";
			listKaryawan.add(new Staff(nama, gaji));
		}
		else if (tipe.equalsIgnoreCase("INTERN")) {
			listKaryawan.add(new Intern(nama, gaji));
		}
		else {
			return "Tidak ada posisi " + tipe;
		}
		return nama + " mulai bekerja sebagai " + tipe + " di PT. TAMPAN";
	}

	public static String status(String nama) {
		Karyawan karyawan = cariKaryawan(nama);
		if (karyawan == null) return "Karyawan tidak ditemukan";
		return nama + " " + karyawan.getGaji();
	}

	public static String addBawahan(String namaCalonAtasan, String namaCalonBawahan) {
		Karyawan calonAtasan = cariKaryawan(namaCalonAtasan);
		Karyawan calonBawahan = cariKaryawan(namaCalonBawahan);
		if (calonAtasan == null || calonBawahan == null) {
			return "Nama tidak berhasil ditemukan";
		}
		return calonAtasan.addBawahan(calonBawahan);
	}

	public static void gajian() {
		for (Karyawan karyawan : listKaryawan) {
			karyawan.gajian();
		}
		System.out.println("Semua karyawan telah diberikan gaji");
		promosi();
	}

	private static void promosi() {
		ArrayList<Karyawan> calonPromosi = new ArrayList<Karyawan>();

		for (Karyawan karyawan : listKaryawan) {
			if (karyawan.getPosisi().equals("STAFF") && karyawan.getGaji() > maxGajiStaff) {
				calonPromosi.add(karyawan);
				System.out.println("Selamat, "+ karyawan.getNama() +" telah dipromosikan menjadi MANAGER");
			}
		}

		for (Karyawan karyawan : calonPromosi) {
			listKaryawan.add(Manager.promosiFromStaff( (Staff)karyawan ));
			listKaryawan.remove(karyawan);
		}
	}
}