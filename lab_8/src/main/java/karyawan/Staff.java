package karyawan;

public class Staff extends Karyawan {

	public Staff(String nama, int gaji) {
		super(nama, gaji);
	}

	public String getPosisi() { return "STAFF"; }

	public boolean canBeBawahan(Karyawan calonBawahan) {
		return bawahan.size() < 10 && calonBawahan.getPosisi().equals("INTERN");
	}
}