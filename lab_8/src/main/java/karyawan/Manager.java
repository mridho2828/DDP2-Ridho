package karyawan;

public class Manager extends Karyawan {

	public Manager(String name, int gaji) {
		super(name, gaji);
	}

	public String getPosisi() { return "MANAGER"; }

	public boolean canBeBawahan(Karyawan calonBawahan) {
		return bawahan.size() < 10 && !calonBawahan.getPosisi().equals("MANAGER");
	}

	public static Manager promosiFromStaff(Staff mantanStaff) {
		Manager manager = new Manager(mantanStaff.getNama(), mantanStaff.getGaji());
		manager.bawahan = mantanStaff.bawahan;
		return manager;
	}
}