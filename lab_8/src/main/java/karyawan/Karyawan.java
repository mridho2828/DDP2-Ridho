package karyawan;

import java.util.ArrayList;

public abstract class Karyawan {

	private String nama;
	private int gaji;
	private int counterGajian = 0;
	protected ArrayList<Karyawan> bawahan;

	public Karyawan(String nama, int gaji) {
		this.nama = nama;
		this.gaji = gaji;
		bawahan = new ArrayList<Karyawan>();
	}

	public String getNama() { return nama; }
	public int getGaji() { return gaji; }

	public String status() {
		return nama + " " + gaji;
	}

	public void gajian() {
		++counterGajian;
		if (counterGajian % 6 == 0) tambahGaji();
	}

	public String addBawahan(Karyawan calonBawahan) {
		if (!canBeBawahan(calonBawahan)) return "Anda tidak layak memiliki bawahan";

		if (cariBawahan(calonBawahan)) {
			return "Karyawan " + calonBawahan.getNama() + " telah menjadi bawahan " + nama;
		}
		else {
			bawahan.add(calonBawahan);
			return "Karyawan " + calonBawahan.getNama() + " berhasil ditambahkan menjadi bawahan " + nama;
		}
	}

	private void tambahGaji() {
		int gajiBaru = gaji + gaji / 10;
		System.out.println(nama + " mengalami kenaikan gaji sebesar 10% dari " + gaji
		+ " menjadi " + gajiBaru);
		gaji = gajiBaru;
	}

	private boolean cariBawahan(Karyawan calonBawahan) {
		for (Karyawan karyawan : bawahan) {
			if (karyawan == calonBawahan) {
				return true;
			}
		}
		return false;
	}

	public abstract String getPosisi();
	public abstract boolean canBeBawahan(Karyawan calonBawahan);
}