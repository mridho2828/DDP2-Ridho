package karyawan;

public class Intern extends Karyawan {

	public Intern(String nama, int gaji) {
		super(nama, gaji);
	}

	public String getPosisi() { return "INTERN"; }

	public boolean canBeBawahan(Karyawan calonBawahan) {
		return false;
	}
}